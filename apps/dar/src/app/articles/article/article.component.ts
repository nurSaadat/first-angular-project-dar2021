import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Article, Category } from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CategoriesService } from '../../categories/categories.service';
import { ArticleService } from '../article.service';

@Component({
  selector: 'dar-lab-ng-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  article: Article;
  categories$: Observable<Category[]>;
  form: FormGroup;

  constructor(
    private articleService: ArticleService,
    private categoriesService: CategoriesService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {

    this.form = this.articleService.createArticleForm();

    this.route.data.subscribe(data => {
      if (data) {
        this.article = data.article;
        this.articleService.patchArticleForm(this.form, data.article);
      }
    })

    this.categories$ = this.categoriesService.getCategories();
  }

  addTag() {
    this.articleService.addTag(this.form)
  }

  removeTag(index: number) {
    this.articleService.removeTag(this.form, index)
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    console.log(this.form.value);

    this.articleService.createArticle(this.form.value)
    .pipe(
      catchError(err => {
        console.log(err);
        return of(null);
      })
    )
    .subscribe(res => {
      console.log(res)
      if (res && res.id) {
        console.log("Success");
        this.articleService.navigateToArticles();
      }
    });
  }
}
