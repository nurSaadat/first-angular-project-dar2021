import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Category } from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CategoriesService } from '../../categories/categories.service';
import { ArticleService } from '../article.service';

@Component({
  selector: 'dar-article-create',
  templateUrl: './article-create.component.html',
  styleUrls: ['./article-create.component.scss']
})
export class ArticleCreateComponent implements OnInit {

  categories$: Observable<Category[]>;
  form: FormGroup;

  constructor(
    private articleService: ArticleService,
    private categoriesService: CategoriesService,
  ) { }

  ngOnInit(): void {
    this.form = this.articleService.createArticleForm();
    this.categories$ = this.categoriesService.getCategories();
  }

  addTag() {
    this.articleService.addTag(this.form)
  }

  removeTag(index: number) {
    this.articleService.removeTag(this.form, index)
  }

  onSubmit() {
    console.log(this.form.value);

    this.articleService.createArticle(this.form.value)
    .pipe(
      catchError(err => {
        console.log(err);
        return of(null);
      })
    )
    .subscribe(res => {
      console.log(res)
      if (res && res.id) {
        console.log("Success");
        this.articleService.navigateToArticles();
      }
    });
  }
}
