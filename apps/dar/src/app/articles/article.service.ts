import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Article } from "@dar-lab-ng/api-interfaces";

@Injectable({ providedIn: 'root' })
export class ArticleService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
  ) { }

  createArticleForm() {
    const form = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.minLength(3)]),
      annotation: new FormControl('', Validators.required),
      is_published: new FormControl(false),
      category_id: new FormControl(''),
      tags: new FormArray([]),
    });

    return form;
  }

  patchArticleForm(form: FormGroup, article: Article) {
    form.patchValue(article)
    article.tags.forEach(tag => {
      const tagsArray = form.get('tags') as FormArray;
      const tagControl = new FormGroup({
        name: new FormControl(tag.name)
      });
      tagsArray.push(tagControl);
    });
  }

  getArticle(id: string) {
    return this.httpClient.get<Article>(`/api/articles/${id}`);
  }

  addTag(form: FormGroup) {
    const tagsArray = form.controls.tags as FormArray;
    const tagControl = new FormGroup({
      name: new FormControl('')
    });
    tagsArray.push(tagControl);
  }

  removeTag(form: FormGroup, index: number) {
    const tagsArray = form.get('tags') as FormArray;
    tagsArray.removeAt(index);
  }

  getArticles(limit: number = 10, sort: string = 'id:DESC') {
    const params: HttpParams = new HttpParams();
    params.set('limit', `${limit}`);
    params.set('sort', sort);

    return this.httpClient
      .get<Article[]>(`/api/articles`, { params })
  }

  createArticle(data: Partial<Article>) {
    return this.httpClient
      .post<Article>(`/api/articles`, data);
  }

  updateArticle(id: string, data: Partial<Article>) {
    return this.httpClient
      .put<Article>(`/api/articles/${id}`, data);
  }

  deleteArticle(id: string) {
    return this.httpClient.delete(`api/articles/${id}`);
  }

  navigateToArticle(id: number) {
    this.router.navigate(['articles', id], {});
  }

  navigateToArticles() {
    this.router.navigate(['/articles']);
  }
}
