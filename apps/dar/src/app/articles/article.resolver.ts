import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router } from "@angular/router";
import { Article } from "@dar-lab-ng/api-interfaces";
import { of } from "rxjs";
import { catchError } from "rxjs/operators";
import { CategoriesService } from "../categories/categories.service";
import { ArticleService } from "./article.service";

@Injectable()
export class ArticleResolver implements Resolve<Article> {
  constructor(
    private articleService: ArticleService,
    private categoriesService: CategoriesService,
  ) { }

    resolve(route: ActivatedRouteSnapshot) {
      const articleId = route.params.id;
      return this.articleService.getArticle(articleId)
      .pipe(catchError(() => {
        this.categoriesService.navigateToError();
        return of(null);
      }))
    }
}
