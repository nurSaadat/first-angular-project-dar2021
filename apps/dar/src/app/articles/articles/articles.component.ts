import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Article } from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, map, mergeMap } from 'rxjs/operators';
import { CategoriesService } from '../../categories/categories.service';
import { ArticleService } from '../article.service';

@Component({
  selector: 'dar-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {

  articles$: Observable<Article[]>;
  searchTerm: FormControl;

  constructor(
    private articleService: ArticleService,
    private categoriesService: CategoriesService,
  ) { }

  ngOnInit(): void {
    this.searchTerm = new FormControl('');

    this.searchTerm
      .valueChanges
      .pipe(
        debounceTime(500)
      )
      .subscribe(term => {
        console.log(term);
        this.getData();
      })

    this.getData();
  }

  getData() {
    this.articles$ = this.articleService.getArticles()
      .pipe(
        catchError(() => of(null)),
        mergeMap(articles =>
          !articles ? of(null) : this.categoriesService.getCategories()
            .pipe(
              map(categories => {
                return articles.map(article => {
                  const category = categories.find(c => c.id === article.category_id);
                  return {
                    ...article,
                    category_title: category ? category.title : 'No category',
                  }
                });
              }),
              catchError(() => of(articles))
            )),
        map((articles: Article[]) => this.searchTerm ? articles.filter(a => a.title.toLowerCase().includes(this.searchTerm.value.toLowerCase())) : articles)
      )
  }

  rowClickHandler(article: Article) {
    this.articleService.navigateToArticle(article.id);
  }

}
