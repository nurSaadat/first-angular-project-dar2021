import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Category } from "@dar-lab-ng/api-interfaces";

@Injectable({ providedIn: 'root' })
export class CategoriesService {
  constructor(
    private httpClient: HttpClient,
    private router: Router,
  ) { }

  createCategoryForm() {
    const form = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.minLength(3)]),
      sort: new FormControl(0, Validators.required),
    })

    return form;
  }

  getCategory(id: string) {
    return this.httpClient
    .get<Category>(`/api/category/${id}`)
  }

  getCategories() {
    return this.httpClient
      .get<Category[]>(`/api/categories`)
  }

  navigateToError() {
    this.router.navigate(["/error"]);
  }

  navigateToCategory(id: number) {
    this.router.navigate(['categories', id], {});
  }
}
