import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CategoriesListComponent } from "./categories-list/categories-list.component";
import { CategoriesRoutingModule } from "./categories-routing.module";
import { CategoriesComponent } from "./categories/categories.component";
import { CategoryResolver } from "./category.resolver";
import { CategoryComponent } from './category/category.component';

@NgModule({
  declarations: [
    CategoriesComponent,
    CategoriesListComponent,
    CategoryComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CategoriesRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [
    CategoryResolver,
  ]
})

export class CategoriesModule { }
