import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Category } from '@dar-lab-ng/api-interfaces';
import { CategoriesService } from '../categories.service';

@Component({
  selector: 'dar-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  category: Category;
  form: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
  ) { }

  ngOnInit(): void {

    this.form = this.categoriesService.createCategoryForm();

    this.route.data.subscribe(data => {
      this.category = data.category;
      this.form.patchValue(this.category);
    });
  }

  onSubmit() {
    if (!this.form.valid){
      return;
    }
    console.log(this.form.value);
  }

}
