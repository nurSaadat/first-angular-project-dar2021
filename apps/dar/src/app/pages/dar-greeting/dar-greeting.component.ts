import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Message } from '@dar-lab-ng/api-interfaces';

@Component({
  selector: 'dar-greeting',
  templateUrl: './dar-greeting.component.html',
  styleUrls: ['./dar-greeting.component.scss']
})

export class DarGreetingComponent implements OnInit{
  public projectName = 'Saadat';
  public greetingText = 'Welcome to';

  constructor() {}

  ngOnInit(): void {
    this.projectName = 'DarLab';
  }

  sayHello(): void {
    this.greetingText = 'Hello, ';
  }
}
