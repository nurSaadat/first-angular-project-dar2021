import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DarGreetingComponent } from './dar-greeting.component';

describe('DarGreetingComponent', () => {
  let component: DarGreetingComponent;
  let fixture: ComponentFixture<DarGreetingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DarGreetingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DarGreetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
