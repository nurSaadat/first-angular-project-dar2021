import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DarGreetingComponent } from "./pages/dar-greeting/dar-greeting.component";
import { NotFoundComponent } from "./not-found/not-found.component";


const routes: Routes = [
  {
    path: 'greetings',
    component: DarGreetingComponent,
  },
  {
    path: 'articles',
    loadChildren: () =>
    import('./articles/articles.module').then(m => m.ArticlesModule),
  },
  {
    path: 'categories',
    loadChildren: () =>
    import('./categories/categories.module').then((m) => m.CategoriesModule),
  },
  {
    path: 'error',
    component: NotFoundComponent,
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'error'
  },

]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ]
})

export class AppRoutingModule {

}
