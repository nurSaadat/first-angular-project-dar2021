export interface Message {
  message: string;
}

export interface Article {
  id: number;
  title: string;
  annotation: string;
  is_published: boolean;
  created_at: string;
  category_id: number;
  category_title?: string;
  tags: Tag[];
}

export interface Category {
  id: number;
  title: string;
  sort: number;
}

export interface Tag {
  name: string;
}
